"""
Infinishop Sync Lite - Lite version of the Prestashop module to sync-up with Infinity, from Visual Software
Copyright (C) 2018  Gioele Renda Popolo (grpeople)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from progress.bar import Bar
from os import system
from libs import config
from libs import infinity_db
from libs import infinishop_db


def sync():
    system('cls')

    configs = config.load()
    infinity = infinity_db.Infinity(configs['INFINITY']['user'], configs['INFINITY']['password'],
                                    configs['INFINITY']['host'], configs['INFINITY']['port'],
                                    configs['INFINITY']['server'], configs['INFINITY']['database'],
                                    configs['INFINITY']['id_catalog'], configs['INFINITY']['customer_type'])
    infinishop = infinishop_db.Infinishop(configs['INFINISHOP']['host'], configs['INFINISHOP']['user'],
                                          configs['INFINISHOP']['password'])
    print()
    print('# Sincronizzazione catalogo')
    print()

    if infinity.test_connection() and infinishop.test_connection():
        print('~ Connessione eseguita correttamente')
        print()
    else:
        print('! Impossibile connettersi\n'
              '! Correggi la configurazione o riprova più tardi')
        print()
        input('Premi invio per continuare')

        return

    # Sync manufacturers
    manufacturers = infinity.get_manufacturers()
    bar = Bar('- Precodici ', max=len(manufacturers), suffix='%(percent)d%%')
    correct = 0
    for i in range(len(manufacturers)):
        if infinishop.post_manufacturer(manufacturers[i]):
            correct += 1
        bar.next()
    bar.finish()
    print('- Precodici sincronizzati: ' + str(correct) + '/' + str(len(manufacturers)))
    print()

    # Sync family groups
    family_groups = infinity.get_family_groups()
    bar = Bar('- Gruppi f. ', max=len(family_groups), suffix='%(percent)d%%')
    correct = 0
    for i in range(len(family_groups)):
        if infinishop.post_family_group(family_groups[i]):
            correct += 1
        bar.next()
    bar.finish()
    print('- Gruppi f. sincronizzati: ' + str(correct) + '/' + str(len(family_groups)))
    print()

    # Sync families
    families = infinity.get_families()
    bar = Bar('- Famiglie  ', max=len(families), suffix='%(percent)d%%')
    correct = 0
    for i in range(len(families)):
        if infinishop.post_family(families[i]):
            correct += 1
        bar.next()
    bar.finish()
    print('- Famiglie sincronizzate: ' + str(correct) + '/' + str(len(families)))
    print()

    # Sync products
    products = infinity.get_products()
    bar = Bar('- Articoli  ', max=len(products), suffix='%(percent)d%%')
    correct = 0
    for i in range(len(products)):
        if infinishop.post_product(products[i]):
            correct += 1
        bar.next()
    bar.finish()
    print('- Articoli sincronizzati: ' + str(correct) + '/' + str(len(products)))
    print()

    print()
    input('Premi invio per continuare')

    return


def edit_config():
    system('cls')

    print()
    print('# Modifica configurazione')

    new_configs = dict()

    print()
    print('- Infinity')
    print()
    user = input('Utente:       ')
    password = input('Password:     ')
    host = input('Host:         ')
    port = input('Port:         ')
    server = input('Server:       ')
    database = input('Database:     ')
    id_catalog = input('ID listino:   ')
    customer_type = input('Tipo cliente: ')

    new_configs['INFINITY'] = {'user': user,
                               'password': password,
                               'host': host,
                               'port': port,
                               'server': server,
                               'database': database,
                               'id_catalog': id_catalog,
                               'customer_type': customer_type}

    print()
    print('- Infinishop')
    print()
    host = input('Host:         ')
    user = input('Utente:       ')
    password = input('Password:     ')

    new_configs['INFINISHOP'] = {'user': user,
                                 'password': password,
                                 'host': host}

    config.save(new_configs)

    print()
    input('Premi invio per continuare')

    return


def install():
    system('cls')

    configs = config.load()
    infinishop = infinishop_db.Infinishop(configs['INFINISHOP']['host'], configs['INFINISHOP']['user'],
                                          configs['INFINISHOP']['password'])

    print()
    print('# Installazione remota in corso')
    print()
    if infinishop.install():
        print('~ Installazione riuscita!')
        print()
    else:
        print('! Installazione fallita!')
        print()

    print()
    input('Premi invio per continuare')

    return


def uninstall():
    system('cls')

    configs = config.load()
    infinishop = infinishop_db.Infinishop(configs['INFINISHOP']['host'], configs['INFINISHOP']['user'],
                                          configs['INFINISHOP']['password'])

    print()
    print('# Disinstallazione')
    print()
    print('! Sei davvero sicuro di voler rimuovere i riferimenti remoti?\n'
          '! I dati eliminati *non potranno* essere ripristinati\n'
          '! Se *SI* scrivi "SONO-SICURISSIMO"\n'
          '! Oppure premi invio per non eseguire modifiche')
    print()
    sure = input('> ')
    if sure == 'SONO-SICURISSIMO':
        print()
        print('- Disinstallazione in corso')
        print()
        if infinishop.uninstall():
            print('~ Disnstallazione riuscita')
        else:
            print('! Disinstallazione fallita')

    print()
    input('Premi invio per continuare')

    return


def first_run():
    system('cls')

    print()
    print('# Benvenuto')
    print()
    print('~ Sembra sia la prima volta che avvi Infinishop Sync.\n'
          '~ Adesso procederemo con la configurazione iniziale\n'
          '~ e l\'installazione remota remota')
    print()
    input('Premi invio per continuare')

    edit_config()
    install()

    return


def menu():
    while True:
        system('cls')

        print()
        print('# Infinishop Sync')
        print()

        print('1) Sincronizza')
        print('2) Modifica configurazione')
        print('0) Esci')
        print()

        choice = input('> ')
        if choice == '1':
            sync()
        if choice == '2':
            edit_config()
        if choice == '999':
            uninstall()
        if choice == '0':
            return
