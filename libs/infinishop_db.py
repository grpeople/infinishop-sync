"""
Infinishop Sync Lite - Lite version of the Prestashop module to sync-up with Infinity, from Visual Software
Copyright (C) 2018  Gioele Renda Popolo (grpeople)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import requests
import requests.auth


class Infinishop(object):
    def __init__(self, host='https://www.example.com/', user='user', password='password'):
        self.__host = host
        self.__user = user
        self.__password = password

    def post_manufacturer(self, manufacturer):
        connection = requests.get(self.__host + '/add-update-manufacturer.php',
                                  auth=requests.auth.HTTPBasicAuth(self.__user, self.__password),
                                  json=manufacturer)

        if connection.status_code == requests.codes.ok and connection.text == 'OK':
            return True
        else:
            return False

    def post_family_group(self, family_group):
        connection = requests.get(self.__host + '/add-update-family-group.php',
                                  auth=requests.auth.HTTPBasicAuth(self.__user, self.__password),
                                  json=family_group)

        if connection.status_code == requests.codes.ok and connection.text == 'OK':
            return True
        else:
            return False

    def post_family(self, family):
        connection = requests.get(self.__host + '/add-update-family.php',
                                  auth=requests.auth.HTTPBasicAuth(self.__user, self.__password),
                                  json=family)

        if connection.status_code == requests.codes.ok and connection.text == 'OK':
            return True
        else:
            return False

    def post_product(self, product):
        connection = requests.get(self.__host + '/add-update-product.php',
                                  auth=requests.auth.HTTPBasicAuth(self.__user, self.__password),
                                  json=product)

        if connection.status_code == requests.codes.ok and connection.text == 'OK':
            return True
        else:
            return False

    def install(self):
        connection = requests.get(self.__host + '/install.php',
                                  auth=requests.auth.HTTPBasicAuth(self.__user, self.__password))

        if connection.status_code == requests.codes.ok and connection.text == 'OK':
            return True
        else:
            return False

    def uninstall(self):
        connection = requests.get(self.__host + '/uninstall.php',
                                  auth=requests.auth.HTTPBasicAuth(self.__user, self.__password))

        if connection.status_code == requests.codes.ok and connection.text == 'OK':
            return True
        else:
            return False

    def test_connection(self):
        connection = requests.get(self.__host + '/test.php',
                                  auth=requests.auth.HTTPBasicAuth(self.__user, self.__password))

        if connection.status_code == requests.codes.ok and connection.text == 'OK':
            return True
        else:
            return False
