"""
Infinishop Sync Lite - Lite version of the Prestashop module to sync-up with Infinity, from Visual Software
Copyright (C) 2018  Gioele Renda Popolo (grpeople)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import sqlanydb as sybase
from datetime import datetime


class Infinity(object):
    def __init__(self, user='user', password='password', host='127.0.0.1', port='2638', server='infinity',
                 database='infinity01', id_catalog='ID', customer_type='CT'):
        # Database connection params
        self.__user = user
        self.__password = password
        self.__host = host
        self.__port = port
        self.__server = server
        self.__database = database
        # Infinity params
        self.__id_catalog = id_catalog
        self.__customer_type = customer_type
        # Database objects
        self.__connection = None
        self.__cursor = None

    def connect(self):
        self.__connection = sybase.connect(uid=self.__user, pwd=self.__password, host=self.__host + ':' + self.__port,
                                           server=self.__server, dbn=self.__database)
        self.__cursor = self.__connection.cursor()

    def disconnect(self):
        self.__cursor.close()
        self.__connection.close()
        self.__connection = None
        self.__cursor = None

    def get_manufacturers(self):
        self.connect()
        sql = 'SELECT DBA.precodici.precodice, DBA.precodici.descrizione ' \
              'FROM DBA.precodici ' \
              'WHERE DBA.precodici.online = 1 ; '
        self.__cursor.execute(sql)
        results = self.__cursor.fetchall()
        self.disconnect()

        return results

    def get_family_groups(self):
        self.connect()
        sql = 'SELECT DBA.gruppo_famiglia.codice, DBA.gruppo_famiglia.descrizione ' \
              'FROM DBA.gruppo_famiglia ; '
        self.__cursor.execute(sql)
        results = self.__cursor.fetchall()
        self.disconnect()

        return results

    def get_families(self):
        self.connect()
        sql = 'SELECT DBA.famiglie.famiglia, DBA.famiglie.gruppo_famiglia, DBA.famiglie.descrizione ' \
              'FROM DBA.famiglie ' \
              'WHERE DBA.famiglie.online = \'S\' ; '
        self.__cursor.execute(sql)
        results = self.__cursor.fetchall()
        self.disconnect()

        return results

    def get_products(self):
        self.connect()
        sql = 'SELECT DBA.articoli.articolo, DBA.articoli.precodice, DBA.articoli.famiglia, DBA.articoli.descrizione,' \
              'DBA.listino_vendita.prezzo, DBA.giacenze.esist_att, DBA.articoli.dimensione_1, ' \
              'DBA.articoli.dimensione_2, DBA.articoli.dimensione_3, DBA.articoli.peso, ' \
              'CASE ' \
              'WHEN DBA.articoli.disp_web LIKE \'1\' THEN 1 ' \
              'WHEN DBA.articoli.disp_web LIKE \'2\' THEN -1 ' \
              'WHEN DBA.articoli.disp_web LIKE \'3\' THEN 0 ' \
              'END AS online ' \
              'FROM DBA.articoli, DBA.giacenze, DBA.listino_vendita ' \
              'WHERE DBA.listino_vendita.id_listino = \'' + self.__id_catalog + '\' ' \
              'AND DBA.listino_vendita.tipo_cliente = \'' + self.__customer_type + '\' ' \
              'AND DBA.giacenze.anno = \'' + str(datetime.now().year) + '\' ' \
              'AND DBA.articoli.articolo = DBA.listino_vendita.articolo ' \
              'AND DBA.articoli.precodice = DBA.listino_vendita.precodice ' \
              'AND DBA.articoli.articolo = DBA.giacenze.articolo ' \
              'AND DBA.articoli.precodice = DBA.giacenze.precodice ;'
        self.__cursor.execute(sql)
        results = self.__cursor.fetchall()
        self.disconnect()

        return results

    def test_connection(self):
        self.connect()
        sql = 'SELECT \'OK\' ; '
        self.__cursor.execute(sql)
        results = self.__cursor.fetchall()[0][0]
        self.disconnect()

        if 'OK' in results:
            return True
        else:
            return False
